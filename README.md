# Playwright/Pytest example reusing authentication

Forked from <https://github.com/automationneemo/POMFrameworkPlaywrightYt>

Modified to reuse authentication, see
- <https://www.youtube.com/watch?v=vYNhPk6OEBI>
- <https://www.youtube.com/watch?v=JMq8ImhDih0>
- <https://www.youtube.com/watch?v=DY_QJTAphtE>

## Dependencies

- pip install pytest-plawright
- pip install pytest
- playwright install

