import pytest
from playwright.sync_api import Playwright, sync_playwright, expect

@pytest.fixture(scope='session')
def create_browser_context(browser) -> None:
    context = browser.new_context()
    page = context.new_page()
    page.goto("https://www.saucedemo.com/")
    page.locator('[data-test="username"]').fill("standard_user")
    page.locator('[data-test="password"]').fill("secret_sauce")
    page.locator('[data-test="login-button"]').click()
    context.storage_state(path="state.json")
    yield context
    context.close()
    pass

@pytest.fixture()
def set_up_tear_down(create_browser_context, browser) -> None:
    context = browser.new_context(storage_state="state.json")
    page = context.new_page()
    page.goto("https://www.saucedemo.com/inventory.html")
    yield page
    context.close()