from pages.LoginPage import LoginPage
from playwright.sync_api import Page, expect

def test_logout(page) -> None:
    page.goto("https://www.saucedemo.com")
    credentials = {'username': 'standard_user', 'password': 'secret_sauce'}
    login_p = LoginPage(page)
    products_p = login_p.do_login(credentials)
    products_p.do_logout()
    expect(login_p.login_button).to_be_visible()